package com.hendisantika.springbootoauth2jwt.repository;

import com.hendisantika.springbootoauth2jwt.entity.Account;
import org.springframework.data.repository.Repository;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2-jwt
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-27
 * Time: 07:14
 * To change this template use File | Settings | File Templates.
 */
public interface AccountRepository extends Repository<Account, Long> {

    Collection<Account> findAll();

    Optional<Account> findByUsername(String username);

    Optional<Account> findById(Long id);

    Integer countByUsername(String username);

    Account save(Account account);

    void deleteAccountById(Long id);

}