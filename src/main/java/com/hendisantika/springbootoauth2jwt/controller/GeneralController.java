package com.hendisantika.springbootoauth2jwt.controller;

import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2-jwt
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-27
 * Time: 07:23
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class GeneralController {

    @GetMapping("/")
    public String home() {
        return "Hello World! " + new Date();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public String create(@RequestBody MultiValueMap<String, String> map) {
        return "OK";
    }

}
